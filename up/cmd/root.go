package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "up",
	Short: "Command Line Interface to manage your UpCloud services",
	Long:  `Command Line Interface to manage your UpCloud services`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Yo-yo. Doing something here.")
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

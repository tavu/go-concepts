package cmd

import (
	"fmt"

	"github.com/UpCloudLtd/upcloud-go-api/upcloud/client"
	"github.com/UpCloudLtd/upcloud-go-api/upcloud/service"
	"github.com/spf13/cobra"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Create and manage servers",
	Long:  `Create and manage servers`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Yo-yo. Doing something here.")
	},
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List servers",
	Long:  `List servers`,
	Run: func(cmd *cobra.Command, args []string) {
		username := "x"
		password := "y"

		svc := service.New(client.New(username, password))

		servers, err := svc.GetServers()

		if err != nil {
			panic(err)
		}

		// Print the UUID and hostname of each server
		for _, server := range servers.Servers {
			fmt.Println(fmt.Sprintf("UUID: %s, hostname: %s", server.UUID, server.Hostname))
		}

		fmt.Println("I'm here")
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
	serverCmd.AddCommand(listCmd)
}

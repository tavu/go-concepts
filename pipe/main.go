package main

//import "fmt"
import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

// Leaf - Demoing io.Writer interface
type Leaf struct {
	Tree string
}

func (s Leaf) Write(p []byte) (n int, err error) {
	//fmt.Println("here at Leaf.Write")
	fmt.Printf("foo: %s", p)
	return len(p), err
}

func main() {
	// CASE: Write output to stdout
	cmd := exec.Command("ls", "-l")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	fmt.Printf("%s\n", strings.Repeat("-", 76))
	cmd.Run()

	// CASE: Write output to file
	myOut, err := os.OpenFile("ls-lR.out", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer myOut.Close()
	cmd = exec.Command("ls", "-lR", "/var/log")
	//cmd.Stdout = os.Stdout
	cmd.Stdout = myOut
	cmd.Stderr = os.Stderr
	fmt.Printf("%s\n", strings.Repeat("-", 76))
	cmd.Run()

	// CASE: Write output to buffer
	var b bytes.Buffer
	cmd = exec.Command("tail", "/var/log/messages")
	cmd.Stdout = &b
	cmd.Stderr = &b
	cmd.Run()
	fmt.Printf("%s\n%s", strings.Repeat("-", 76), b.String())

	myleaf := Leaf{
		Tree: "Koivu",
	}
	cmd = exec.Command("tail", "-f", "/tmp/output")
	cmd.Stdout = myleaf
	fmt.Printf("%s\n", strings.Repeat("-", 76))
	cmd.Run()
	/*out, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println(err)
	}*/
	/*err = cmd.Start()
	if err != nil {
		fmt.Println(err)
	}
	cmd.Wait()*/
	//cmd.Stdout = os.Stdout;
	//cmd.Stderr = os.Stderr;
	//out := cmd.Run()
	/*if err != nil {
		fmt.Println(err)
	}*/
	//fmt.Println(out)
	//fmt.Println(cmd.Stdout)
}

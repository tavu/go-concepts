package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

// Person This is the custom struct from our package
type Person struct {
	ID   int
	Name string
	Age  int
}

func (p *Person) Write(w io.Writer) {
	b, _ := json.Marshal(*p)
	_, err := w.Write(b)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	me := Person{
		ID:   1,
		Name: "Me",
		Age:  64,
	}
	var b bytes.Buffer
	me.Write(&b)
	fmt.Printf("%s", b.String())

	myOut, err := os.OpenFile("person.out", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := myOut.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	me.Write(myOut)
}

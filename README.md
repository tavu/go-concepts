# Go Concepts

[![Build Status](https://travis-ci.org/7ojo/go-concepts.svg?branch=master)](https://travis-ci.org/7ojo/go-concepts)

Testing different concepts with Golang. Summarizing stuff from books, blogs, web etc to get overall understanding of different strategies and best practices.

## Editors

### VIM

- go-vim
    * https://blog.gopheracademy.com/vimgo-development-environment/
    * https://github.com/fatih/vim-go
    * "gofmt on save"
    * Commands like :GoBuild :GoTest :GoTestFunc() :GoRun :GoDoc
    * Completion support
    * etc

## CI (Continuous Integration

### Travis CI

File: `.travis.yml`
```
language: go
go: "1.10.x"
# script: go test -v ./...
```

### GitLab CI

File: `.gitlab-ci.yml`
```
before_script:
  - export PROJECT_NAME=github.com/7ojo/go-concepts
  - export GOPATH=/go-work
  - mkdir -pv $GOPATH/{bin,pkg,src}
  - mkdir -pv $GOPATH/src/$PROJECT_NAME
  - cp -rv $CI_PROJECT_DIR/* $GOPATH/src/$PROJECT_NAME

stages:
  - test
  # - build
  # - deploy

test-go-1_10:
  image: golang:1.10-alpine
  stage: test
  script:
    - go test -v ./...
```

## Interesting Packages

* FileSystem Abstraction - https://github.com/spf13/afero
  - https://stackoverflow.com/questions/43912124/example-code-for-testing-the-filesystem-in-golang#43914455
  - https://stackoverflow.com/questions/16742331/how-to-mock-abstract-filesystem-in-go
* CLI Apps with Go - https://github.com/urfave/cli
* SFTP support for the go.crypto/ssh package - https://github.com/pkg/sftp
* A Commander for modern Go CLI interactions - https://github.com/spf13/cobra
* Go configuration with fangs - https://github.com/spf13/viper

## Projects

### hello

Concepts:
- Unit testing on main and hello packages
- TableDrivenTests: https://github.com/golang/go/wiki/TableDrivenTests

Files:
```
├── command.go
├── command_test.go
└── hello
    ├── hello.go
    └── hello_test.go
```

Install:
```
$ go install
```

Tests:
```
go test -v ./...
=== RUN   TestSaySomething
--- PASS: TestSaySomething (0.00s)
PASS
ok      github.com/7ojo/go-concepts/hello   (cached)
=== RUN   TestHello
--- PASS: TestHello (0.00s)
PASS
ok      github.com/7ojo/go-concepts/hello/hello (cached)
```

Output:
```
$ hello
Hello, World
```

### iam

Concepts:
- Debian packaging. Simplified. Works.

Files:
```
├── debian
│   ├── changelog
│   ├── compat
│   ├── control
│   ├── copyright
│   ├── files
│   ├── rules
│   └── source
│       └── format
├── iam.go
└── Makefile
```

Install:
```
$ make deb
(lots of lines)
$ sudo dpkg -i ../iam_0.1-1_amd64.deb
```

Output:
```
$ iam
i am tojo
```


package main

import (
	"os/exec"
	"testing"
)

func TestSaySomething(t *testing.T) {
	cases := []struct {
		in, want string
	}{
		{"", "Hello, World"},
	}
	for _, c := range cases {
		s := saySomething()
		if s != c.want {
			t.Errorf("saySomething(%q) != %q, want %q", c.in, s, c.want)
		}
	}
}

// Disabled test. First thought was to test compiled version of it, but probably
// not so good idea do in the long run to do it here :thinking: at least it is
// not very common to do so.
func _TestHelloCLI(t *testing.T) {
	// TableDrivenTests: https://github.com/golang/go/wiki/TableDrivenTests
	cases := []struct {
		in, want string
	}{
		{"hello-cli", "Hello, World\n"},
	}
	for _, c := range cases {
		got, err := exec.Command(c.in).Output()
		s := string(got[:])
		if err != nil || s != c.want {
			t.Errorf("$(%q) != %q, want %q", c.in, s, c.want)
		}
	}
}

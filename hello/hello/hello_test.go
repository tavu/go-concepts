package hello

import "testing"

func TestHello(t *testing.T) {
	if v := Hello("World"); v != "Hello, World" {
		t.Errorf("Expected 'Hello, World', but got '%s'", v)
	}
}

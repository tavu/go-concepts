package main

import (
	"fmt"

	"github.com/7ojo/go-concepts/hello/hello"
)

func saySomething() string {
	return hello.Hello("World")
}

func main() {
	fmt.Println(saySomething())
}
